#!/bin/bash

for file in *.sty; do
    iconv -f utf-8 -t utf-8 "$file" -o "${file%.sty}"
done
