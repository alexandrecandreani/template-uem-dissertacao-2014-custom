exclude=*.ps *.brf *.acr *.alg *.acn *.ist *.lof *.lot *~ *.gls *.glsdefs  *.aux  *.nlo *.glo *.bbl \
*.blg *.log  *.out *.bak* *.toc *.glg *.ind *.nls *.idx  *.ilg *.xd
all:	
	make mess
mess:
	pdflatex -src -interaction=nonstopmode main.tex
#	makeindex main.idx
#	makeindex main.nlo -s nomencl.ist -o main.nls
	bibtex main.aux	
	makeglossaries main
	pdflatex   -src -interaction=nonstopmode main.tex
	make main
main:
	pdflatex -synctex=1 -src -interaction=nonstopmode main.tex 
	rm -fr **/*.gls **/*~ **/*.aux **/*.bbl **/*.nlo **/*.glo \
**/*.blg **/*.log **/*.out **/*.bak* **/*.toc
	rm  -fr $(exclude)

update:
	make clean 
	git add .
ifneq ($(strip $(c)),)	
	git commit -am "$(c)"
else 
	git commit -am "update"
endif
	
	git push -u origin master
	 

clean:
	rm -fr **/*.lof **/*.lot **/*~ **/*.aux **/*.dvi \
**/*.bbl **/*.blg **/*.log   **/*.out **/*.bak* **/*.toc **/*.nlo **/*.glo
	rm -fr $(exclude) main.pdf *.synctex.gz
